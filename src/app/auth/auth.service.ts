import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { Subject } from "rxjs";

import { AuthData } from "./auth-data.model";
import { nullSafeIsEquivalent } from "@angular/compiler/src/output/output_ast";
import Utils from "../utils";

/*Authentication functionality service*/
@Injectable({ providedIn: "root" })
export class AuthService {
  private backend_url: string = Utils.getBackendURL();  private isAuthenticated = false;
  private fullName = null;
  private token: string;
  private tokenTimer: any;
  private userId: string;
  private authStatusListener = new Subject<boolean>();
  private fullNameListener = new Subject<string>();

  constructor(private http: HttpClient, private router: Router) {}

  getToken() {
    return this.token;
  }

  getIsAuth() {
    return this.isAuthenticated;
  }
  getFullName() {
    return this.fullName;
  }
  getUserId() {
    return this.userId;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }
  getFullNameListener() {
    return this.fullNameListener.asObservable();
  }

  createUser(email: string, password: string,firstname:string,lastname:string) {
    const authData: AuthData = { email: email, password: password,firstname:firstname, lastname:lastname };
    this.http
      .post(`${this.backend_url}api/user/signup`, authData)
      .subscribe(() => {
        this.router.navigate(["/"]);
      }, error => {
        this.authStatusListener.next(false);
        this.fullNameListener.next(null);
      });
  }

  login(email: string, password: string) {
    const authData: AuthData = { email: email, password: password,firstname: '', lastname: '' } ;
    this.http
      .post<{ token: string; expiresIn: number; userId: string,firstname:string,lastname:string }>(
        `${this.backend_url}api/user/login`,
        authData
      )
      .subscribe(response => {
        const token = response.token;
        this.token = token;
        if (token) {
          const expiresInDuration = response.expiresIn;
          this.setAuthTimer(expiresInDuration);
          this.isAuthenticated = true;
          this.fullName = response.firstname +' '+ response.lastname;
          this.userId = response.userId;
          this.authStatusListener.next(true);
          this.fullNameListener.next(this.fullName);
          const now = new Date();
          const expirationDate = new Date(
            now.getTime() + expiresInDuration * 1000
          );
          console.log(expirationDate);
          this.saveAuthData(token, expirationDate, this.userId,response.firstname,response.lastname);
          this.router.navigate(["/missions"]);
        }
      }, error => {
        this.authStatusListener.next(false);
        this.fullNameListener.next(null);
      });
  }

  autoAuthUser() {
    const authInformation = this.getAuthData();
    if (!authInformation) {
      return;
    }
    const now = new Date();
    const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
    if (expiresIn > 0) {
      this.token = authInformation.token;
      this.isAuthenticated = true;
      this.fullName = authInformation.firstname +' '+ authInformation.lastname;
      this.userId = authInformation.userId;
      this.setAuthTimer(expiresIn / 1000);
      this.authStatusListener.next(true);
      this.fullNameListener.next(this.fullName);
    }
  }

  logout() {
    this.token = null;
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.fullNameListener.next(null);
    this.userId = null;
    this.fullName = null;
    clearTimeout(this.tokenTimer);
    this.clearAuthData();
    this.router.navigate(["/"]);
  }

  private setAuthTimer(duration: number) {
    console.log("Setting timer: " + duration);
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 1000);
  }

  private saveAuthData(token: string, expirationDate: Date, userId: string,firstname:string,lastname:string) {
    localStorage.setItem("token", token);
    localStorage.setItem("expiration", expirationDate.toISOString());
    localStorage.setItem("userId", userId);
    localStorage.setItem("firstname", firstname);
    localStorage.setItem("lastname", lastname);
  }

  private clearAuthData() {
    localStorage.removeItem("token");
    localStorage.removeItem("expiration");
    localStorage.removeItem("userId");
    localStorage.removeItem("firstname");
    localStorage.removeItem("lastname");
  }

  private getAuthData() {
    const token = localStorage.getItem("token");
    const expirationDate = localStorage.getItem("expiration");
    const userId = localStorage.getItem("userId");
    const firstname = localStorage.getItem("firstname");
    const lastname = localStorage.getItem("lastname");
    if (!token || !expirationDate) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userId: userId,
      firstname: firstname,
      lastname :lastname
    };
  }
}
