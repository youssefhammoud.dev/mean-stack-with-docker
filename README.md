## Whats Including In This Repository
We have implemented below **MEAN stack application that create missions and transactions with CRUD functions**.


See the overall picture of **implementations a mean stack applications** on real-world project;


![datei](/Docs/mean-stack-flow.png)

The MEAN stack is a full-stack, JavaScript-based framework for developing web applications. MEAN stands for MongoDB Express Angular Node, after the four key technologies that make up the different layers of the stack. 

## Run The Project
You will need the following tools:

* [Visual Studio Code](https://code.visualstudio.com/download/)
* [Docker](https://docs.docker.com/get-docker/)

### Installing
Follow these steps to get your development environment set up: (Before Run Start the Docker Desktop)

1. Before starting watch the videos - [videos](videos/)

2. Clone the repository

3. Set up your IP address in - [utils.ts](/src/app/utils.ts) ==> (to pass as environment variable in a later stage)

4. At the root directory which include **docker-compose.yml** files, run below command to create a mongo DB / backend / and frontend instances:
```csharp
docker-compose up -d
```
5. Wait for docker compose. That’s it! (it takes about 12 minutes for the first time, on the next time, docker will cashe the dependencies)

6. Launch http://{your-ip}:4200/ in your browser to view the Web UI.


![datei](/Docs/login.PNG)

# How the application work ? 
As all application you should **SignUp** then **SignIn**.

## Create Mission:
![datei](/Docs/addnewmission.PNG)

## Add Transaction to the mission
![datei](/Docs/addtransaction.PNG)

## List all missions
![datei](/Docs/listmissions.PNG)

## Next step you can update delete mission or transaction, and you can use the pagination to dividing the potential result into pages and retrieving the required pages, one by one on demand.




Our Database is composed from two Schema (User And Mission)
-----------------------------------------------------------

![datei](/Docs/MongoDB.png)

**User Schema** :
```json
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": {},
    "examples": [
        {
            "_id": "600f5a18b2a96e1b1c236748",
            "email": "jojo2@hotmail.com",
            "firstname": "Youssef",
            "lastname": "Hammoud",
            "password": "$2b$10$NaO2PEWeEizQ.VsEcugWqOM8T3VDyBDapTy9f2MHnrPeDmWIaQve2",
            "__v": 0
        }
    ],
    "required": [
        "_id",
        "email",
        "firstname",
        "lastname",
        "password",
        "__v"
    ],
    "properties": {
        "_id": {
            "$id": "#/properties/_id",
            "type": "string",
            "title": "The _id schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "600f5a18b2a96e1b1c236748"
            ]
        },
        "email": {
            "$id": "#/properties/email",
            "type": "string",
            "title": "The email schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "jojo2@hotmail.com"
            ]
        },
        "firstname": {
            "$id": "#/properties/firstname",
            "type": "string",
            "title": "The firstname schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Youssef"
            ]
        },
        "lastname": {
            "$id": "#/properties/lastname",
            "type": "string",
            "title": "The lastname schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "Hammoud"
            ]
        },
        "password": {
            "$id": "#/properties/password",
            "type": "string",
            "title": "The password schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "$2b$10$NaO2PEWeEizQ.VsEcugWqOM8T3VDyBDapTy9f2MHnrPeDmWIaQve2"
            ]
        },
        "__v": {
            "$id": "#/properties/__v",
            "type": "integer",
            "title": "The __v schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                0
            ]
        }
    },
    "additionalProperties": true
}
```
---


**Mission Schema** :
```json
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "array",
    "title": "The root schema",
    "description": "The root schema comprises the entire JSON document.",
    "default": [],
    "examples": [
        [
            {
                "_id": "600f5a29b2a96e1b1c236749",
                "title": "gjghjghjghjh",
                "status": "Pending Approval",
                "creator": "600f5a18b2a96e1b1c236748",
                "transactions": [
                    {
                        "_id": "600f5a37b2a96e1b1c23674a",
                        "id": "7498034c-2e28-49e3-a53a-889df3ae8557",
                        "date": "2021-01-26T00:00:00.000Z",
                        "typeOfFees": "commissions!",
                        "label": "1000",
                        "amount": 54545,
                        "imagePath": "http://localhost:3000/images/7498034c-2e28-49e3-a53a-889df3ae8557-1611618881523.jpg",
                        "transactionType": "Cash",
                        "description": null
                    }
                ],
                "__v": 0
            }
        ]
    ],
    "additionalItems": true,
    "items": {
        "$id": "#/items",
        "anyOf": [
            {
                "$id": "#/items/anyOf/0",
                "type": "object",
                "title": "The first anyOf schema",
                "description": "An explanation about the purpose of this instance.",
                "default": {},
                "examples": [
                    {
                        "_id": "600f5a29b2a96e1b1c236749",
                        "title": "gjghjghjghjh",
                        "status": "Pending Approval",
                        "creator": "600f5a18b2a96e1b1c236748",
                        "transactions": [
                            {
                                "_id": "600f5a37b2a96e1b1c23674a",
                                "id": "7498034c-2e28-49e3-a53a-889df3ae8557",
                                "date": "2021-01-26T00:00:00.000Z",
                                "typeOfFees": "commissions!",
                                "label": "1000",
                                "amount": 54545,
                                "imagePath": "http://localhost:3000/images/7498034c-2e28-49e3-a53a-889df3ae8557-1611618881523.jpg",
                                "transactionType": "Cash",
                                "description": null
                            }
                        ],
                        "__v": 0
                    }
                ],
                "required": [
                    "_id",
                    "title",
                    "status",
                    "creator",
                    "transactions",
                    "__v"
                ],
                "properties": {
                    "_id": {
                        "$id": "#/items/anyOf/0/properties/_id",
                        "type": "string",
                        "title": "The _id schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "600f5a29b2a96e1b1c236749"
                        ]
                    },
                    "title": {
                        "$id": "#/items/anyOf/0/properties/title",
                        "type": "string",
                        "title": "The title schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "gjghjghjghjh"
                        ]
                    },
                    "status": {
                        "$id": "#/items/anyOf/0/properties/status",
                        "type": "string",
                        "title": "The status schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "Pending Approval"
                        ]
                    },
                    "creator": {
                        "$id": "#/items/anyOf/0/properties/creator",
                        "type": "string",
                        "title": "The creator schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": "",
                        "examples": [
                            "600f5a18b2a96e1b1c236748"
                        ]
                    },
                    "transactions": {
                        "$id": "#/items/anyOf/0/properties/transactions",
                        "type": "array",
                        "title": "The transactions schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": [],
                        "examples": [
                            [
                                {
                                    "_id": "600f5a37b2a96e1b1c23674a",
                                    "id": "7498034c-2e28-49e3-a53a-889df3ae8557",
                                    "date": "2021-01-26T00:00:00.000Z",
                                    "typeOfFees": "commissions!",
                                    "label": "1000",
                                    "amount": 54545,
                                    "imagePath": "http://localhost:3000/images/7498034c-2e28-49e3-a53a-889df3ae8557-1611618881523.jpg",
                                    "transactionType": "Cash",
                                    "description": null
                                }
                            ]
                        ],
                        "additionalItems": true,
                        "items": {
                            "$id": "#/items/anyOf/0/properties/transactions/items",
                            "anyOf": [
                                {
                                    "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0",
                                    "type": "object",
                                    "title": "The first anyOf schema",
                                    "description": "An explanation about the purpose of this instance.",
                                    "default": {},
                                    "examples": [
                                        {
                                            "_id": "600f5a37b2a96e1b1c23674a",
                                            "id": "7498034c-2e28-49e3-a53a-889df3ae8557",
                                            "date": "2021-01-26T00:00:00.000Z",
                                            "typeOfFees": "commissions!",
                                            "label": "1000",
                                            "amount": 54545,
                                            "imagePath": "http://localhost:3000/images/7498034c-2e28-49e3-a53a-889df3ae8557-1611618881523.jpg",
                                            "transactionType": "Cash",
                                            "description": null
                                        }
                                    ],
                                    "required": [
                                        "_id",
                                        "id",
                                        "date",
                                        "typeOfFees",
                                        "label",
                                        "amount",
                                        "imagePath",
                                        "transactionType",
                                        "description"
                                    ],
                                    "properties": {
                                        "_id": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/_id",
                                            "type": "string",
                                            "title": "The _id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "600f5a37b2a96e1b1c23674a"
                                            ]
                                        },
                                        "id": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/id",
                                            "type": "string",
                                            "title": "The id schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "7498034c-2e28-49e3-a53a-889df3ae8557"
                                            ]
                                        },
                                        "date": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/date",
                                            "type": "string",
                                            "title": "The date schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "2021-01-26T00:00:00.000Z"
                                            ]
                                        },
                                        "typeOfFees": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/typeOfFees",
                                            "type": "string",
                                            "title": "The typeOfFees schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "commissions!"
                                            ]
                                        },
                                        "label": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/label",
                                            "type": "string",
                                            "title": "The label schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "1000"
                                            ]
                                        },
                                        "amount": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/amount",
                                            "type": "integer",
                                            "title": "The amount schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": 0,
                                            "examples": [
                                                54545
                                            ]
                                        },
                                        "imagePath": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/imagePath",
                                            "type": "string",
                                            "title": "The imagePath schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "http://localhost:3000/images/7498034c-2e28-49e3-a53a-889df3ae8557-1611618881523.jpg"
                                            ]
                                        },
                                        "transactionType": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/transactionType",
                                            "type": "string",
                                            "title": "The transactionType schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": "",
                                            "examples": [
                                                "Cash"
                                            ]
                                        },
                                        "description": {
                                            "$id": "#/items/anyOf/0/properties/transactions/items/anyOf/0/properties/description",
                                            "type": "null",
                                            "title": "The description schema",
                                            "description": "An explanation about the purpose of this instance.",
                                            "default": null,
                                            "examples": [
                                                null
                                            ]
                                        }
                                    },
                                    "additionalProperties": true
                                }
                            ]
                        }
                    },
                    "__v": {
                        "$id": "#/items/anyOf/0/properties/__v",
                        "type": "integer",
                        "title": "The __v schema",
                        "description": "An explanation about the purpose of this instance.",
                        "default": 0,
                        "examples": [
                            0
                        ]
                    }
                },
                "additionalProperties": true
            }
        ]
    }
}
```
---
## Author

* **Youssef Hammoud** - *Initial work* - [youssefhammoud](https://gitlab.com/youssefhammoud/mean-stack-application)