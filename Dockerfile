### STAGE 1: Build ###
FROM node:10.19.0 AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . /usr/src/app
EXPOSE 4200
CMD ["npm", "start"]
